//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Initialize.rc
//
#define IDC_MYICON                      2
#define IDR_MENU1                       101
#define IDD_ASSIGNMENT_3_DIALOG         102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_ASSIGNMENT_3                107
#define IDI_SMALL                       108
#define IDC_ASSIGNMENT_3                109
#define IDM_CONNECT                     110
#define IDM_COM1                        111
#define IDM_COM2                        112
#define IDM_COM3                        113
#define IDM_COM4                        114
#define IDM_COM5                        115
#define IDM_COM6                        116
#define IDM_HELP                        117
#define IDM_STOP                        118
#define IDM_EXIT                        119
#define IDM_SAVE                        120
#define IDM_CLEAR                       121
#define IDR_MAINFRAME                   128


// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
