/*------------------------------------------------------------------------------------------------------------------
-- SOURCE FILE: Initialize.cpp - Initializes the window and registers it. It also displays the menubar which has
--                               various options. It is also responsible for handing the
--                               window handle to the Physical layer object and
--                               the hComm handle to the Session layer object.
--
-- PROGRAM:     Assignment_2 RFID Reader
--
-- FUNCTIONS:   int WINAPI       WinMain(HINSTANCE hInst, HINSTANCE hprevInstance, LPSTR lspszCmdParam, int nCmdShow)
--              LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
--				int deviceText(HWND hwnd);
--				int appendText(HWND hwnd);
--				int saveNewFile(HWND hwnd);
--              int clearText(HWND hwnd);
--				DWORD WINAPI init(LPVOID lpParam);
--				void close(HANDLE& thread);
--				unsigned char threadFunction(LPSKYETEK_TAG lpTag, void *user);
--
-- DATE:        November 13, 2016
--
-- DESIGNER:    Michael Goll | Mark Chen
--
-- PROGRAMMER:  Michael Goll | Mark Chen
--
-- NOTES:
----------------------------------------------------------------------------------------------------------------------*/
#ifndef INITIALIZE_H
#define INITIALIZE_H

//Windows header
#include <windows.h>

//C++ headers
#include <stdio.h>
#include <stdlib.h>
#include <string>

//SkyeTek API Headers
#include "../../API/SkyeTekAPI.h"
#include "../../API/SkyeTekProtocol.h"

//Event message declarations
#define IDM_CONNECT      110
#define IDM_COM1         111
#define IDM_COM2         112
#define IDM_COM3         113
#define IDM_COM4         114
#define IDM_COM5         115
#define IDM_COM6         116
#define IDM_HELP         117
#define IDM_STOP         118
#define IDM_EXIT         119
#define IDM_SAVE         120
#define IDM_CLEAR        121


//Initialize.cpp function prototypes
/*------------------------------------------------------------------------------------------------------------------
-- SOURCE FILE: Initialize.cpp - Initializes the window and registers it. It also displays the menubar which has
--                               various options. It is also responsible for handing the
--                               window handle to the Physical layer object and
--                               the hComm handle to the Session layer object.
--
-- PROGRAM:     Assignment_2 RFID Reader
--
-- FUNCTIONS:   int WINAPI       WinMain(HINSTANCE hInst, HINSTANCE hprevInstance, LPSTR lspszCmdParam, int nCmdShow)
--              LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
--				int deviceText(HWND);
--				int appendText(HWND);
--				int saveNewFile(HWND);
--				DWORD WINAPI init(LPVOID);
--
-- DATE:        November 13, 2016
--
-- DESIGNER:    Michael Goll | Mark Chen
--
-- PROGRAMMER:  Michael Goll | Mark Chen
--
-- NOTES:
----------------------------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   WinMain
--
-- DATE:       November 12, 2016
--
-- DESIGNER:   Mark Chen | Michael Goll
--
-- PROGRAMMER: Mark Chen | Michael Goll
--
-- INTERFACE:  int WinMain(GINSTANCE, HINSTANCE, LPSTR, int)
--
-- RETURNS:    int
--
-- NOTES:
-- Creating the main window that will contain the program.
--
----------------------------------------------------------------------------------------------------------------------*/
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int);

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   WndProc
--
-- DATE:       November 12, 2016
--
-- DESIGNER:   Mark Chen | Michael Goll
--
-- PROGRAMMER: Mark Chen | Michael Goll
--
-- INTERFACE:  LRESULT WndProc(HWND, UINT, WPARAM, LPARAM)
--
-- RETURNS:    LRESULT
--
-- NOTES:
-- Performs the following tasks based on the messages received:
-- - Creating the additional child windows (Text log, device status)
-- - Creating the additional buttons (Save, Clear, Connect, Disconnect)
-- - Invoking the approprite methods for each menu option/button
--
-- Start button allows the scanner to begin receiving signals. The stop button will disconnect the RFID scanner and not allow
-- the program to receive any further signals. The clear button clears the log screen. The save button allows for the user
-- to save what is currently in the log screen to a file on their harddrive.
----------------------------------------------------------------------------------------------------------------------*/
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION: saveNewFile
--
-- DATE: November 12, 2016
--
-- DESIGNER: http://www.cplusplus.com/forum/windows/92689/ (Design by user dwaseem and reimplemented to our program)
--
-- PROGRAMMER: Mark Chen | Michael Goll
--
-- INTERFACE: int saveNewFile(HWND)
--
-- RETURNS: int
--
-- NOTES:
-- This function allows the user to save what is currently in the text box that contains the log of devices that have
-- been scanned.
--
----------------------------------------------------------------------------------------------------------------------*/
int saveNewFile(HWND);
int clearText(HWND);

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION: appendText
--
-- DATE: November 12, 2016
--
-- DESIGNER: Mark Chen | Michael Goll
--
-- PROGRAMMER: Mark Chen | Michael Goll
--
-- INTERFACE: int appendText(HWND, LPCSTR)
--
-- RETURNS: int
--
-- NOTES:
-- Appends text to the end of what is currently inside the Device log text box.
--
----------------------------------------------------------------------------------------------------------------------*/
int appendText(HWND, LPCSTR);

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION: deviceText
--
-- DATE: November 12, 2016
--
-- DESIGNER: Mark Chen | Michael Goll
--
-- PROGRAMMER: Mark Chen | Michael Goll
--
-- INTERFACE: int deviceText(HWND, LPCSTR)
--
-- RETURNS: int
--
-- NOTES:
-- Updates the text box for RFID scanner status with the current status of the RFID scanner.
--
----------------------------------------------------------------------------------------------------------------------*/
int deviceText(HWND, LPCTSTR);

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   clearText
--
-- DATE:       November 15, 2016
--
-- DESIGNER:   Mark Chen
--
-- PROGRAMMER: Mark Chen
--
-- INTERFACE: int clearText(HWND)
--
-- RETURNS:   int
--
-- NOTES:
-- Clears the tag textbox contents. Affects the save function's output.
--
----------------------------------------------------------------------------------------------------------------------*/
int clearText(HWND hwnd);


//Session.cpp function prototypes
/*------------------------------------------------------------------------------------------------------------------
-- SOURCE FILE : Session.cpp -  Handles the Session layer of the application. Searches for SkyeTek devices connected
--                              to the computer. Initializes a connection session if a device and reader are found
--                              to read RFID tags. Also handles closing of the session if the reader is unplugged
--                              during execution of the user terminates the session.
--
-- PROGRAM : Assignment_2 RFID Reader
--
-- FUNCTIONS :  DWORD WINAPI init(LPVOID lpParam)
--              void close(HANDLE& thread)
--              void setDeviceText(HWND hwnd)
--
-- DATE:        November 13, 2016
--
-- DESIGNER :   Michael Goll
--
-- PROGRAMMER : Michael Goll
--
-- NOTES : init will almost always find devices connected to the computer as it searches through USB and other
--         various connections but will only initiate a session if the device contains a RFID reader.
----------------------------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION: init
--
-- DATE: November 13, 2016
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- RETURNS:    DWORD
--
-- INTERFACE: DWORD WINAPI init(LPVOID)
--
-- NOTES:
-- Searches for all devices attached to the computer. If some devices are found, it loops through them and scans for
-- any RFID readers. If a SkyeTek reader is found, the device name is printed to the UI and the reader is initialized
-- in loop mode for reading RFID tags. If no readers are found, the user interface is updated accordingly.
--
----------------------------------------------------------------------------------------------------------------------*/
DWORD WINAPI init(LPVOID lpParam);

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION: close
--
-- DATE: November 13, 2016
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- RETURNS:    void
--
-- INTERFACE:  void close(HANDLE&)
--
-- NOTES:
-- Closes the reading thread if it exists and frees all devices and readers in the device and reader arrays.
--
----------------------------------------------------------------------------------------------------------------------*/
void close(HANDLE& thread);

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION: setDeviceText
--
-- DATE: November 13, 2016
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- RETURNS:    void
--
-- INTERFACE:  void setDeviceText(HWND)
--
-- NOTES:
-- Initializes the deviceListSession handle to the device textbox. Used to update the device textbox on the user
-- interface.
--
----------------------------------------------------------------------------------------------------------------------*/
void setDeviceText(HWND hwnd);

//Physical.cpp function prototypes
/*------------------------------------------------------------------------------------------------------------------
-- SOURCE FILE : Physical.cpp - Handles the Physical layer of the application. Handles the reading of tags and
--                              printing them to the tag textbox on the user interface.
--
-- PROGRAM :    Assignment_2 RFID Reader
--
-- FUNCTIONS :  threadFunction(LPSKYETEK_TAG, void*)
--              void setAppendList(HWND) {
--
-- DATE:        November 13, 2016
--
-- DESIGNER :   Michael Goll
--
-- PROGRAMMER : Michael Goll
--
-- NOTES:
----------------------------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   threadFunction
--
-- DATE:       November 13, 2016
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- RETURNS:    unsigned char
--
-- INTERFACE:  unsigned char threadFunction(LPSKYETEK_TAG, void*)
--
-- NOTES:
-- Called by the RFID reader when a tag is scanned. Prints the tag's type and the friendly separated by a hyphen.
-- Prints a new line after each full tag is read. The function sleeps for 1 second after printing a tag to prevent
-- the user interface updating too quickly and becoming unreadable.
----------------------------------------------------------------------------------------------------------------------*/
unsigned char threadFunction(LPSKYETEK_TAG lpTag, void *user);

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   setAppendList
--
-- DATE:       November 13, 2016
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- RETURNS:    unsigned char
--
-- INTERFACE:  void setAppendList(HWND)
--
-- NOTES:
-- Sets the handle to the tag textbox to allow printing to the user interface.
----------------------------------------------------------------------------------------------------------------------*/
void setAppendList(HWND);

#endif