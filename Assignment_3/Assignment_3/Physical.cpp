/*------------------------------------------------------------------------------------------------------------------
-- SOURCE FILE : Physical.cpp - Handles the Physical layer of the application. Handles the reading of tags and
--                              printing them to the tag textbox on the user interface.
--
-- PROGRAM :    Assignment_2 RFID Reader
--
-- FUNCTIONS :  threadFunction(LPSKYETEK_TAG, void*)
--              void setAppendList(HWND) {
--
-- DATE:        November 13, 2016
--
-- DESIGNER :   Michael Goll
--
-- PROGRAMMER : Michael Goll
--
-- NOTES:
----------------------------------------------------------------------------------------------------------------------*/


#include "stdafx.h"
#include "Initialize.h"

HDC hdc;
TCHAR tagTypeBuffer[256];
TCHAR idBuffer[128] = _T("Hi");
bool stopped = FALSE;
int listCounter = 0;
HWND appendList;

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   threadFunction
--
-- DATE:       November 13, 2016
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- RETURNS:    unsigned char
--
-- INTERFACE:  unsigned char threadFunction(LPSKYETEK_TAG, void*)
--
-- NOTES:
-- Called by the RFID reader when a tag is scanned. Prints the tag's type and the friendly separated by a hyphen. 
-- Prints a new line after each full tag is read. The function sleeps for 1 second after printing a tag to prevent
-- the user interface updating too quickly and becoming unreadable. 
----------------------------------------------------------------------------------------------------------------------*/
unsigned char threadFunction(LPSKYETEK_TAG lpTag, void *user) {
	std::string friendlyString;
	size_t j = 0;

	if (!stopped) {
		if (lpTag != NULL) {

			appendText(appendList, LPCSTR(SkyeTek_GetTagTypeNameFromType(lpTag->type)));
			appendText(appendList, LPCSTR("-"));
			appendText(appendList, LPCSTR(lpTag->friendly));
			appendText(appendList, LPCSTR("\n"));

			SkyeTek_FreeTag(lpTag);
			Sleep(1000);
		}
	}
	return (!stopped);
}


/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   setAppendList
--
-- DATE:       November 13, 2016
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- RETURNS:    unsigned char
--
-- INTERFACE:  void setAppendList(HWND)
--
-- NOTES:
-- Sets the handle to the tag textbox to allow printing to the user interface. 
----------------------------------------------------------------------------------------------------------------------*/
void setAppendList(HWND hwnd) {
	appendList = hwnd;
}



