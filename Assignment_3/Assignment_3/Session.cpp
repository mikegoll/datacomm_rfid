/*------------------------------------------------------------------------------------------------------------------
-- SOURCE FILE : Session.cpp -  Handles the Session layer of the application. Searches for SkyeTek devices connected 
--                              to the computer. Initializes a connection session if a device and reader are found 
--                              to read RFID tags. Also handles closing of the session if the reader is unplugged
--                              during execution of the user terminates the session. 
--
-- PROGRAM :    Assignment_2 RFID Reader
--
-- FUNCTIONS :  DWORD WINAPI init(LPVOID)
--              void close(HANDLE&)
--              void setDeviceText(HWND)
--
-- DATE:        November 13, 2016
--
-- DESIGNER :   Michael Goll
--
-- PROGRAMMER : Michael Goll
--
-- NOTES : init will almost always find devices connected to the computer as it searches through USB and other
--         various connections but will only initiate a session if the device contains a RFID reader.
----------------------------------------------------------------------------------------------------------------------*/

#include "stdafx.h"
#include "Initialize.h"

LPSKYETEK_DEVICE *devices = NULL;
LPSKYETEK_READER *readers = NULL;
int numDevices = 0;
int numReaders = 0;
HWND deviceListSession = NULL;

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION: init
--
-- DATE: November 13, 2016
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- RETURNS:    DWORD
--
-- INTERFACE: DWORD WINAPI init(LPVOID)
--
-- NOTES:
-- Searches for all devices attached to the computer. If some devices are found, it loops through them and scans for
-- any RFID readers. If a SkyeTek reader is found, the device name is printed to the user interface and the reader 
-- is initialized in loop mode for reading RFID tags. If no readers are found, the user interface is updated 
-- accordingly.
--
----------------------------------------------------------------------------------------------------------------------*/

DWORD WINAPI init(LPVOID lpParam) {

	deviceText(deviceListSession, L"Finding Devices...");

	//Discovers all devices plugged into the host
	if ((numDevices = SkyeTek_DiscoverDevices(&devices)) == 0) {
		deviceText(deviceListSession, L"No Devices Found.");
	}

	deviceText(deviceListSession, L"Finding Readers...");

	//Discovers readers in all of the discovered devices
	if ((numReaders = SkyeTek_DiscoverReaders(devices, numDevices, &readers)) == 0) {
		deviceText(deviceListSession, L"No Reader Found.");
		Sleep(2000);
	}

	if (numReaders) {
		//Prints out the device name to the "Devices" textbox
		deviceText(deviceListSession, LPCTSTR(readers[0]->readerName));

		//Runs the reader in loop mode
		SkyeTek_SelectTags(readers[0], AUTO_DETECT, threadFunction, 0, 1, NULL);		
	}

	SkyeTek_FreeDevices(devices, numDevices);
	SkyeTek_FreeReaders(readers, numReaders);

	//Restoring default text on UI
	deviceText(deviceListSession, L"[Devices Appear Here]");

	return 0;
}


/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION: close
--
-- DATE: November 13, 2016
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- RETURNS:    void
--
-- INTERFACE:  void close(HANDLE&)
--
-- NOTES:
-- Closes the reading thread if it exists and frees all devices and readers in the device and reader arrays.
--
----------------------------------------------------------------------------------------------------------------------*/

void close(HANDLE& thread) {
	if (thread != NULL) {
		TerminateThread(thread, 0);

		SkyeTek_FreeDevices(devices, numDevices);
		SkyeTek_FreeReaders(readers, numReaders);
	}
}

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION: setDeviceText
--
-- DATE: November 13, 2016
--
-- DESIGNER:   Michael Goll
--
-- PROGRAMMER: Michael Goll
--
-- RETURNS:    void
--
-- INTERFACE:  void setDeviceText(HWND)
--
-- NOTES:
-- Initializes the deviceListSession handle to the device textbox. Used to update the device textbox on the user
-- interface.
--
----------------------------------------------------------------------------------------------------------------------*/

void setDeviceText(HWND hwnd) {
	deviceListSession = hwnd;
}

