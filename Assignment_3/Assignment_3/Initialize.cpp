/*------------------------------------------------------------------------------------------------------------------
-- SOURCE FILE: Initialize.cpp - Initializes the window and registers it. It also displays the menubar which has
--                               various options. It is also responsible for handing the
--                               window handle to the Physical layer object and
--                               the hComm handle to the Session layer object.
--
-- PROGRAM:     Assignment_2 RFID Reader
--
-- FUNCTIONS:   int WINAPI       WinMain(HINSTANCE hInst, HINSTANCE hprevInstance, LPSTR lspszCmdParam, int nCmdShow)
--              LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
--				int deviceText(HWND);
--				int appendText(HWND);
--				int saveNewFile(HWND);
--				DWORD WINAPI init(LPVOID);
--
-- DATE:        November 13, 2016
--
-- DESIGNER:    Michael Goll | Mark Chen
--
-- PROGRAMMER:  Michael Goll | Mark Chen
--
-- NOTES:
----------------------------------------------------------------------------------------------------------------------*/

//Win32 Headers
#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <ShObjIdl.h>

//SkyeTek API Headers
#include "../../API/SkyeTekAPI.h"
#include "../../API/SkyeTekProtocol.h"

//Custom headers
#include "resource.h"
#include "Initialize.h"

#pragma warning (disable: 4096)

#pragma comment(linker,"\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

static TCHAR Name[] = TEXT("RFID Scanner | Mark Chen | Michael Goll");
char   str[80] = "";
TCHAR   menuName[] = TEXT("MAINUI");
HANDLE hComm;
HANDLE threadHandle = NULL;
DWORD  threadExitCode;

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   WinMain
--
-- DATE:       November 12, 2016
--
-- DESIGNER:   Mark Chen | Michael Goll
--
-- PROGRAMMER: Mark Chen | Michael Goll
--
-- INTERFACE:  int WinMain(GINSTANCE, HINSTANCE, LPSTR, int)
--
-- RETURNS:    int
--
-- NOTES:
-- Creating the main window that will contain the program.
--
----------------------------------------------------------------------------------------------------------------------*/

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hprevInstance, LPSTR lspszCmdParam, int nCmdShow) {
	HWND       hwnd;
	MSG        Msg;
	WNDCLASSEX Wcl;

	Wcl.cbSize = sizeof(WNDCLASSEX);
	Wcl.style = CS_HREDRAW | CS_VREDRAW;
	Wcl.hIcon = LoadIcon(NULL, MAKEINTRESOURCE(IDI_APPLICATION));
	Wcl.hIconSm = NULL;
	Wcl.hCursor = LoadCursor(NULL, IDC_ARROW);

	Wcl.lpfnWndProc = WndProc;
	Wcl.hInstance = hInst;
	Wcl.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	Wcl.lpszClassName = Name;

	Wcl.lpszMenuName = menuName;
	Wcl.cbClsExtra = 0;
	Wcl.cbWndExtra = 0;
	Wcl.hbrBackground = CreateSolidBrush(0x002E2D2D);  // This line colours the window

	if (!RegisterClassEx(&Wcl)) {
		return 0;
	}

	hwnd = CreateWindow(Name, Name, WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX, 50, 50, 800, 440, NULL, NULL, hInst, NULL);
	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);

	while (GetMessage(&Msg, NULL, 0, 0)) {
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
	}

	return Msg.wParam;
}

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   WndProc
--
-- DATE:       November 12, 2016
--
-- DESIGNER:   Mark Chen | Michael Goll
--
-- PROGRAMMER: Mark Chen | Michael Goll
--
-- INTERFACE:  LRESULT WndProc(HWND, UINT, WPARAM, LPARAM)
--
-- RETURNS:    LRESULT
--
-- NOTES:
-- Performs the following tasks based on the messages received:
-- - Creating the additional child windows (Text log, device status)
-- - Creating the additional buttons (Save, Clear, Connect, Disconnect)
-- - Invoking the approprite methods for each menu option/button
--
-- Start button allows the scanner to begin receiving signals. The stop button will disconnect the RFID scanner and not allow
-- the program to receive any further signals. The clear button clears the log screen. The save button allows for the user
-- to save what is currently in the log screen to a file on their harddrive. 
----------------------------------------------------------------------------------------------------------------------*/

LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam) {

	PAINTSTRUCT ps;
	HDC hdc;
	HANDLE threadHandle = NULL;
	DWORD tID;

	setDeviceText(hwnd);
	setAppendList(hwnd);

	switch (Message) {

	case WM_CREATE:
	{
		HWND hEdit = CreateWindowEx(WS_EX_CLIENTEDGE,
			L"EDIT",
			TEXT(""),
			WS_CHILD | WS_VISIBLE | WS_VSCROLL | ES_MULTILINE | ES_AUTOVSCROLL,
			15,
			15,
			550,
			325,
			hwnd,
			NULL,
			NULL,
			NULL);

		// Creating the textbox to list Devices, TODO: make static (Mark)
		HWND hDevice = CreateWindowEx(WS_EX_CLIENTEDGE,
			L"EDIT",
			TEXT("[Devices Appear Here]"),
			WS_CHILD | WS_VISIBLE |
			ES_MULTILINE | ES_AUTOVSCROLL | ES_AUTOHSCROLL,
			580,
			15,
			190,
			50,
			hwnd,
			NULL,
			NULL,
			NULL);


		// Creating a Save Button
		HWND hWndSave = CreateWindowEx(NULL,
			L"BUTTON",
			L"Save",
			WS_TABSTOP | WS_VISIBLE |
			WS_CHILD | BS_DEFPUSHBUTTON,
			353,
			350,
			100,
			24,
			hwnd,
			(HMENU)IDM_SAVE,
			NULL,
			NULL);

		// Creating a Clear Button
		HWND hWndClear = CreateWindowEx(NULL,
			L"BUTTON",
			L"Clear",
			WS_TABSTOP | WS_VISIBLE |
			WS_CHILD | BS_DEFPUSHBUTTON,
			238,
			350,
			100,
			24,
			hwnd,
			(HMENU)IDM_CLEAR,
			NULL,
			NULL);

		// Creating a Connect Button
		HWND hWndConnect = CreateWindowEx(NULL,
			L"BUTTON",
			L"Connect",
			WS_TABSTOP | WS_VISIBLE |
			WS_CHILD | BS_DEFPUSHBUTTON,
			15,
			350,
			100,
			24,
			hwnd,
			(HMENU)IDM_CONNECT,
			NULL,
			NULL);

		// Creating a Stop Button
		HWND hWndStop = CreateWindowEx(NULL,
			L"BUTTON",
			L"Stop",
			WS_TABSTOP | WS_VISIBLE |
			WS_CHILD | BS_DEFPUSHBUTTON,
			126,
			350,
			100,
			24,
			hwnd,
			(HMENU)IDM_STOP,
			NULL,
			NULL);

		// Creating a Help Button
		HWND hWndHelp = CreateWindowEx(NULL,
			L"BUTTON",
			L"Help",
			WS_TABSTOP | WS_VISIBLE |
			WS_CHILD | BS_DEFPUSHBUTTON,
			465,
			350,
			100,
			24,
			hwnd,
			(HMENU)IDM_HELP,
			NULL,
			NULL);
	}
	break;

	case WM_COMMAND:
		switch (LOWORD(wParam)) {
		case IDM_CONNECT:
			if (threadHandle == NULL)
				threadHandle = CreateThread(NULL, 0, init, NULL, 0, &tID);
			else {
				close(threadHandle);
				MessageBox(NULL, TEXT("Unable to start program."), TEXT("Program Error"), MB_ICONEXCLAMATION | MB_OK);
			}
				
			break;

		case IDM_STOP:
			deviceText(hwnd, L"Device Disconnecting.");
			close(threadHandle);
			deviceText(hwnd, L"Device Disconnected.");
			Sleep(500);
			deviceText(hwnd, L"[Devices Appear Here]");
			break;

		case IDM_HELP:
			MessageBox(NULL, TEXT("Make sure the scanner is plugged in.\nClick \"Connect\" and wait for the RFID scanner to be found.\nOnce found, the device name will appear in the upper right textbox and you may begin scanning tags."), TEXT("RFID Help"), MB_ICONINFORMATION | MB_OK);
			break;

		case IDM_SAVE:
			saveNewFile(hwnd);
			break;

		case IDM_CLEAR:
			clearText(hwnd);
			break;

		case IDM_EXIT:
			close(threadHandle);
			DestroyWindow(hwnd);
			break;

		case IDM_CLEAR:
			clearText(hwnd);
			break;
		}
		break;

	case WM_CTLCOLOREDIT:
	{
		HDC hdc = (HDC)wParam;
		SetTextColor(hdc, 0x00FFFFFF);  // Sets Text Colour to White
		SetBkColor(hdc, 0xFF000000);    // Sets Text background to Black
		return (LRESULT)GetStockObject(BLACK_BRUSH); // return a Black brush, which makes full textbox Black
	}
	break;

	case WM_CTLCOLOREDIT:    // Changing the background colour of the Edit boxes
	{
		HDC hdc = (HDC)wParam;
		SetTextColor(hdc, 0x00FFFFFF);  // Sets Text Colour to White
		SetBkColor(hdc, 0xFF000000);    // Sets Text background to Black
		return (LRESULT)GetStockObject(BLACK_BRUSH); // return a Black brush, which makes full textbox Black
	}
	break;

	case WM_DESTROY:		// message to terminate the program
		close(threadHandle);
		PostQuitMessage(0);
		break;

	default: // Let Win32 process all other messages
		return DefWindowProc(hwnd, Message, wParam, lParam);
	}
	return 0;
}

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION: saveNewFile
--
-- DATE: November 12, 2016
--
-- DESIGNER: http://www.cplusplus.com/forum/windows/92689/ (Design by user dwaseem and reimplemented to our program)
--
-- PROGRAMMER: Mark Chen | Michael Goll
--
-- INTERFACE: int saveNewFile(HWND)
--
-- RETURNS: int
--
-- NOTES:
-- This function allows the user to save what is currently in the text box that contains the log of devices that have 
-- been scanned. 
--
----------------------------------------------------------------------------------------------------------------------*/
int saveNewFile(HWND hwnd)
{

	HWND hEdit = GetWindow(hwnd, GW_CHILD);

	int length = (GetWindowTextLength(hEdit)) + 1;
	wchar_t *fileText = new wchar_t[length];
	GetWindowText(hEdit, fileText, length);
	DWORD dwWritten; // number of bytes written to file
	HANDLE hFile;  //file handle

	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED |
		COINIT_DISABLE_OLE1DDE);

	if (SUCCEEDED(hr))
	{
		IFileSaveDialog *pFileSave;

		// Create the FileOpenDialog object.
		hr = CoCreateInstance(CLSID_FileSaveDialog, NULL, CLSCTX_ALL,
			IID_IFileSaveDialog, reinterpret_cast<void**>(&pFileSave));

		if (SUCCEEDED(hr))
		{
			// Show the Open dialog box.
			hr = pFileSave->Show(NULL);

			// Get the file name from the dialog box.
			if (SUCCEEDED(hr))
			{
				IShellItem *pItem;
				hr = pFileSave->GetResult(&pItem);
				if (SUCCEEDED(hr))
				{
					PWSTR pszFilePath;
					hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);

					if (SUCCEEDED(hr))
					{
						hFile = CreateFile(pszFilePath, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
						if (hFile == INVALID_HANDLE_VALUE)
						{
							MessageBox(0, TEXT("Could not create/open a file"), TEXT("Error"), 16);
							return 1;
						}
						WriteFile(hFile, fileText, length * 2, &dwWritten, 0);
						CloseHandle(hFile);
					}
					pItem->Release();
				}
			}
			pFileSave->Release();
		}
		CoUninitialize();
	}
	return 0;
}

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION: appendText
--
-- DATE: November 12, 2016
--
-- DESIGNER: Mark Chen | Michael Goll
--
-- PROGRAMMER: Mark Chen | Michael Goll
--
-- INTERFACE: int appendText(HWND, LPCSTR)
--
-- RETURNS: int
--
-- NOTES:
-- Appends text to the end of what is currently inside the Device log text box by sending messages to windows about
-- the current length of what is in the edit box and then appending using EM_SETSEL and EM_REPLACESEL.
--
----------------------------------------------------------------------------------------------------------------------*/

int appendText(HWND hwnd, LPCSTR newMessage)
{
	HWND hEdit = GetWindow(hwnd, GW_CHILD);
	int length = (GetWindowTextLength(hEdit)) + 1;
	LPCTSTR message = L"\r";  // This is the what message gets appened to the end of file currently, the "\r\n" combo creates a new line for windows
	SetFocus(hEdit);
	std::string newBuffer;

	SendMessage(hEdit, EM_SETSEL, (WPARAM)length, (LPARAM)length);
	SendMessage(hEdit, EM_REPLACESEL, 0, (LPARAM)(LPCSTR)newMessage);
	SendMessage(hEdit, EM_REPLACESEL, 0, (LPARAM)(LPCSTR)message);

	return 0;
}


/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION: deviceText
--
-- DATE: November 12, 2016
--
-- DESIGNER: Mark Chen | Michael Goll
--
-- PROGRAMMER: Mark Chen | Michael Goll
--
-- INTERFACE: int deviceText(HWND, LPCSTR)
--
-- RETURNS: int
--
-- NOTES:
-- Updates the text box for RFID scanner status with the current status of the RFID scanner using SetWindowText to
-- replace whatever is currently in the edit box.
-- 
----------------------------------------------------------------------------------------------------------------------*/

int deviceText(HWND hwnd, LPCTSTR newMessage)
{
	HWND hDevice = GetWindow(hwnd, GW_CHILD);
	hDevice = GetWindow(hDevice, GW_HWNDNEXT);

	SetWindowText(hDevice, newMessage);

	return 0;
}

/*------------------------------------------------------------------------------------------------------------------
-- FUNCTION:   clearText
--
-- DATE:       November 15, 2016
--
-- DESIGNER:   Mark Chen
--
-- PROGRAMMER: Mark Chen
--
-- INTERFACE: int clearText(HWND)
--
-- RETURNS:   int
--
-- NOTES:
-- Clears the tag textbox contents. Affects the save function's output. 
--
----------------------------------------------------------------------------------------------------------------------*/
int clearText(HWND hwnd)
{
	HWND hEdit = GetWindow(hwnd, GW_CHILD);
	LPCTSTR message = L"";

	SetWindowText(hEdit, message);

	return 0;
}
